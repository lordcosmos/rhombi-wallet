# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     EWallet.Repo.insert!(%EWallet.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
EWallet.Accounts.create_user(%{email: "user@test.com", password: "12345678", name: "Test User"})
