defmodule EWallet.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :available_balance, :integer
      add :current_balance, :integer
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create index(:accounts, [:user_id])
  end
end
