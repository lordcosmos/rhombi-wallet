defmodule EWallet.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    execute(
      "create type transactions_type as enum ('load', 'void', 'refund', 'pre-auth', 'sale', 'release')"
    )

    create table(:transactions) do
      add :description, :string
      add :type, :transactions_type
      add :amount, :integer
      add :account_id, references(:accounts, on_delete: :nothing)
      add :reference_id, references(:transactions, on_delete: :nothing)

      timestamps()
    end

    create index(:transactions, [:account_id])
  end
end
