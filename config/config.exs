# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :e_wallet,
  ecto_repos: [EWallet.Repo]

# Configures the endpoint
config :e_wallet, EWalletWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "CCGTQTAx91ia2OIT6L2fuFw0WV+wwj8GuVi3bHNdjdXh0ld1NiRz5mR8msTZ4r2W",
  render_errors: [view: EWalletWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: EWallet.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures the authentication
config :e_wallet,
  token_salt: "pK8Nud1qLkLr553y8SKSetjGosc-BF",
  seed: "user token"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
