defmodule EWallet.AccountsTest do
  use EWallet.DataCase

  alias EWallet.Accounts

  alias EWallet.Accounts.User
  alias EWallet.Accounts.Account
  alias EWallet.Accounts.Transaction

  @valid_user_attrs %{email: "some email", name: "some name"}
  @invalid_user_attrs %{email: nil, name: nil}

  @valid_account_attrs %{}
  @invalid_account_attrs %{}

  @valid_transaction_attrs %{credit: "120.5", debit: "120.5", description: "some description"}
  @invalid_transaction_attrs %{credit: nil, debit: nil, description: nil}

  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(@valid_user_attrs)
      |> Accounts.create_user()

    user
  end

  def account_fixture(attrs \\ %{}) do
    {:ok, account} =
      attrs
      |> Enum.into(@valid_account_attrs)
      |> Accounts.create_account()

    account
  end

  def transaction_fixture(attrs \\ %{}) do
    {:ok, transaction} =
      attrs
      |> Enum.into(@valid_transaction_attrs)
      |> Accounts.create_transaction()

    transaction
  end

  describe "users" do
    # test "list_users/0 returns all users" do
    #   user = user_fixture()
    #   assert Accounts.list_users() == [user]
    # end

    # test "get_user!/1 returns the user with given id" do
    #   user = user_fixture()
    #   assert Accounts.get_user!(user.id) == user
    # end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_user_attrs)
      assert user.email == "some email"
      assert user.name == "some name"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_user_attrs)
    end

    # test "update_user/2 with valid data updates the user" do
    #   user = user_fixture()
    #   assert {:ok, %User{} = user} = Accounts.update_user(user, @update_attrs)
    #   assert user.email == "some updated email"
    #   assert user.name == "some updated name"
    # end

    # test "update_user/2 with invalid data returns error changeset" do
    #   user = user_fixture()
    #   assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
    #   assert user == Accounts.get_user!(user.id)
    # end

    # test "delete_user/1 deletes the user" do
    #   user = user_fixture()
    #   assert {:ok, %User{}} = Accounts.delete_user(user)
    #   assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    # end

    # test "change_user/1 returns a user changeset" do
    #   user = user_fixture()
    #   assert %Ecto.Changeset{} = Accounts.change_user(user)
    # end
  end

  describe "accounts" do
    # test "list_accounts/0 returns all accounts" do
    #   account = account_fixture()
    #   assert Accounts.list_accounts() == [account]
    # end

    test "get_account!/1 returns the account with given id" do
      account = account_fixture()
      assert Accounts.get_account!(account.id) == account
    end

    test "create_account/1 with valid data creates a account" do
      user = user_fixture()
      assert {:ok, %Account{} = account} = Accounts.create_account(user, @valid_attrs)
      assert account.available_balance == 0
      assert account.current_balance == 0
    end

    test "create_account/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_account(%{}, @invalid_attrs)
    end

    # test "update_account/2 with valid data updates the account" do
    #   account = account_fixture()
    #   assert {:ok, %Account{} = account} = Accounts.update_account(account, @update_attrs)
    #   assert account.balance == Decimal.new("456.7")
    #   assert account.number == "some updated number"
    # end

    # test "update_account/2 with invalid data returns error changeset" do
    #   account = account_fixture()
    #   assert {:error, %Ecto.Changeset{}} = Accounts.update_account(account, @invalid_attrs)
    #   assert account == Accounts.get_account!(account.id)
    # end

    # test "delete_account/1 deletes the account" do
    #   account = account_fixture()
    #   assert {:ok, %Account{}} = Accounts.delete_account(account)
    #   assert_raise Ecto.NoResultsError, fn -> Accounts.get_account!(account.id) end
    # end

    # test "change_account/1 returns a account changeset" do
    #   account = account_fixture()
    #   assert %Ecto.Changeset{} = Accounts.change_account(account)
    # end
  end

  describe "transactions" do
    # test "list_transactions/0 returns all transactions" do
    #   transaction = transaction_fixture()
    #   assert Accounts.list_transactions() == [transaction]
    # end

    test "get_transaction!/1 returns the transaction with given id" do
      transaction = transaction_fixture()
      assert Accounts.get_transaction!(transaction.id) == transaction
    end

    test "create_transaction/1 with valid data creates a transaction" do
      assert {:ok, %Transaction{} = transaction} = Accounts.create_transaction(@valid_attrs)
      assert transaction.credit == Decimal.new("120.5")
      assert transaction.debit == Decimal.new("120.5")
      assert transaction.description == "some description"
    end

    test "create_transaction/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_transaction(@invalid_attrs)
    end

    # test "update_transaction/2 with valid data updates the transaction" do
    #   transaction = transaction_fixture()

    #   assert {:ok, %Transaction{} = transaction} =
    #            Accounts.update_transaction(transaction, @update_attrs)

    #   assert transaction.credit == Decimal.new("456.7")
    #   assert transaction.debit == Decimal.new("456.7")
    #   assert transaction.description == "some updated description"
    # end

    # test "update_transaction/2 with invalid data returns error changeset" do
    #   transaction = transaction_fixture()

    #   assert {:error, %Ecto.Changeset{}} =
    #            Accounts.update_transaction(transaction, @invalid_attrs)

    #   assert transaction == Accounts.get_transaction!(transaction.id)
    # end

    # test "delete_transaction/1 deletes the transaction" do
    #   transaction = transaction_fixture()
    #   assert {:ok, %Transaction{}} = Accounts.delete_transaction(transaction)
    #   assert_raise Ecto.NoResultsError, fn -> Accounts.get_transaction!(transaction.id) end
    # end

    # test "change_transaction/1 returns a transaction changeset" do
    #   transaction = transaction_fixture()
    #   assert %Ecto.Changeset{} = Accounts.change_transaction(transaction)
    # end
  end
end
