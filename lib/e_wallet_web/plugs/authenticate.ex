defmodule EWalletWeb.Plugs.Authenticate do
  import Plug.Conn
  def init(default), do: default

  def call(conn, _default) do
    case EWallet.Services.Authenticator.get_auth_token(conn) do
      {:ok, token} ->
        case EWallet.Repo.get_by(EWallet.AuthToken, %{token: token, revoked: false})
             |> EWallet.Repo.preload(:user) do
          nil -> unauthorized(conn)
          auth_token -> authorized(conn, auth_token.user)
        end

      _ ->
        unauthorized(conn)
    end
  end

  defp authorized(conn, user) do
    conn
    |> assign(:signed_in, true)
    |> assign(:current_user, user)
  end

  defp unauthorized(conn) do
    conn |> send_resp(401, "Unauthorized") |> halt()
  end
end
