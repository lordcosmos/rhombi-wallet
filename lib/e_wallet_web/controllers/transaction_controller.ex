defmodule EWalletWeb.TransactionController do
  use EWalletWeb, :controller

  alias EWallet.Accounts
  alias EWallet.Accounts.Transaction

  action_fallback EWalletWeb.FallbackController

  def index(conn, _params) do
    transactions = Accounts.list_transactions()
    render(conn, "index.json", transactions: transactions)
  end

  def create(conn, %{"type" => type, "amount" => _amount, "account_id" => account_id} = params)
      when type in ["pre-auth", "load", "sale", "void", "refund"] do
    account = Accounts.get_account!(account_id)
    IO.inspect(account)
    IO.inspect(params)

    with {:ok, %Transaction{} = transaction} <- Accounts.create_transaction(account, params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.transaction_path(conn, :show, transaction))
      |> render("show.json", transaction: transaction)
    else
      {:error, reason} ->
        conn |> put_status(:bad_request) |> render("error.json", message: reason)
    end
  end

  def create(conn, _params),
    do: conn |> put_status(:bad_request) |> render("error.json", message: "Bad request")

  def show(conn, %{"id" => id}) do
    transaction = Accounts.get_transaction!(id)
    render(conn, "show.json", transaction: transaction)
  end
end
