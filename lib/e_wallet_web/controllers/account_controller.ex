defmodule EWalletWeb.AccountController do
  use EWalletWeb, :controller

  alias EWallet.Accounts
  alias EWallet.Accounts.Account

  action_fallback EWalletWeb.FallbackController

  def create(conn, _params) do
    with {:ok, %Account{} = account} <- Accounts.create_account(conn.assigns.current_user) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.account_path(conn, :show, account))
      |> render("show.json", account: account)
    end
  end

  def show(conn, %{"id" => id}) do
    account = Accounts.get_account!(id)
    render(conn, "show.json", account: account)
  end
end
