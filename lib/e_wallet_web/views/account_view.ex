defmodule EWalletWeb.AccountView do
  use EWalletWeb, :view
  alias EWalletWeb.AccountView

  def render("index.json", %{accounts: accounts}) do
    %{data: render_many(accounts, AccountView, "account.json")}
  end

  def render("show.json", %{account: account}) do
    %{data: render_one(account, AccountView, "account.json")}
  end

  def render("account.json", %{account: account}) do
    available = "$" <> Float.to_string(account.available_balance / 100)
    current = "$" <> Float.to_string(account.current_balance / 100)
    %{account_number: account.id, available_balance: available, current_balance: current}
  end
end
