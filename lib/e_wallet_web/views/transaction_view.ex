defmodule EWalletWeb.TransactionView do
  use EWalletWeb, :view
  alias EWalletWeb.TransactionView

  def render("index.json", %{transactions: transactions}) do
    %{data: render_many(transactions, TransactionView, "transaction.json")}
  end

  def render("show.json", %{transaction: transaction}) do
    %{data: render_one(transaction, TransactionView, "transaction.json")}
  end

  def render("error.json", %{message: message}), do: %{data: message}

  def render("transaction.json", %{transaction: transaction}) do
    %{
      id: transaction.id,
      description: transaction.description,
      type: transaction.type,
      amount: "$" <> Float.to_string(transaction.amount / 100),
      account_id: transaction.account_id
    }
  end
end
