defmodule EWalletWeb.SessionView do
  use EWalletWeb, :view
  alias EWalletWeb.AccountView

  def render("show.json", %{token: token}) do
    %{data: %{token: token}}
  end
end
