defmodule EWalletWeb.Router do
  use EWalletWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authenticate do
    plug EWalletWeb.Plugs.Authenticate
  end

  scope "/api", EWalletWeb do
    pipe_through :api

    post "/sign_in", SessionController, :create
    delete "/sign_out", SessionController, :delete

    resources "/transactions", TransactionController, only: [:create, :show]
  end

  scope "/api", EWalletWeb do
    pipe_through :api
    pipe_through :authenticate

    resources "/accounts", AccountController, only: [:create, :show]
  end
end
