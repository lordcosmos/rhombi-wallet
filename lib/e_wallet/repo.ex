defmodule EWallet.Repo do
  use Ecto.Repo,
    otp_app: :e_wallet,
    adapter: Ecto.Adapters.Postgres
end
