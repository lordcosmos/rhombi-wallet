defmodule EWallet.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias EWallet.Services.Authenticator

  schema "users" do
    field :email, :string
    field :name, :string
    field :password_hash, :string
    field :password, :string, virtual: true

    timestamps()

    has_many :accounts, EWallet.Accounts.Account, on_replace: :delete, on_delete: :delete_all
    has_many :transactions, through: [:accounts, :transactions]
    has_many :auth_tokens, EWallet.AuthToken
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :email, :password])
    |> cast_assoc(:accounts)
    |> validate_required([:name, :email, :password])
    |> unique_constraint(:email, downcase: true)
    |> put_password_hash()
  end

  def sign_in(email, password) do
    case Comeonin.Bcrypt.check_pass(EWallet.Repo.get_by(__MODULE__, email: email), password) do
      {:ok, user} ->
        token = Authenticator.generate_token(user)
        EWallet.Repo.insert(Ecto.build_assoc(user, :auth_tokens, %{token: token}))

      err ->
        err
    end
  end

  def sign_out(conn) do
    case Authenticator.get_auth_token(conn) do
      {:ok, token} ->
        case EWallet.Repo.get_by(AuthToken, %{token: token}) do
          nil -> {:error, :not_found}
          auth_token -> EWallet.Repo.delete(auth_token)
        end

      error ->
        error
    end
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(pass))

      _ ->
        changeset
    end
  end
end
