defmodule EWallet.Accounts.Account do
  use Ecto.Schema
  import Ecto.Changeset

  # Account should be wallet
  schema "accounts" do
    field :available_balance, :integer
    field :current_balance, :integer
    belongs_to :user, EWallet.Accounts.User

    timestamps()

    has_many :transactions, EWallet.Accounts.Transaction
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:available_balance, :current_balance])
    |> validate_required([:available_balance, :current_balance])
  end
end
