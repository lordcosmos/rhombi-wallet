defmodule EWallet.Accounts.Transaction do
  use Ecto.Schema
  import Ecto.Changeset

  schema "transactions" do
    field :description, :string
    field :amount, :integer
    field :type, :string
    belongs_to :account, EWallet.Accounts.Account
    belongs_to :reference, EWallet.Accounts.Transaction

    timestamps()
  end

  @doc false
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:description, :amount, :type])
    |> validate_inclusion(:type, ["load", "void", "refund", "pre-auth", "sale", "release"])
    |> validate_required([:description, :amount, :type])
  end
end
