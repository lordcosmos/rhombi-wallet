defmodule EWallet.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  import Ecto.Changeset, only: [change: 2]
  alias EWallet.Repo

  alias EWallet.Accounts.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  # def list_users do
  #   Repo.all(User)
  # end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  # def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  # def update_user(%User{} = user, attrs) do
  #   user
  #   |> User.changeset(attrs)
  #   |> Repo.update()
  # end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  # def delete_user(%User{} = user) do
  #   Repo.delete(user)
  # end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  # def change_user(%User{} = user) do
  #   User.changeset(user, %{})
  # end

  alias EWallet.Accounts.Account

  @doc """
  Returns the list of accounts.

  ## Examples

      iex> list_accounts()
      [%Account{}, ...]

  """
  # def list_accounts do
  #   Repo.all(Account)
  # end

  @doc """
  Gets a single account.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get_account!(123)
      %Account{}

      iex> get_account!(456)
      ** (Ecto.NoResultsError)

  """
  def get_account!(id), do: Repo.get!(Account, id)

  @doc """
  Creates a account.

  ## Examples

      iex> create_account(%{field: value})
      {:ok, %Account{}}

      iex> create_account(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_account(%User{} = current_user) do
    attrs = %{available_balance: 0, current_balance: 0}
    changeset = Account.changeset(%Account{}, attrs)

    if(changeset.valid?) do
      Ecto.build_assoc(current_user, :accounts, attrs)
      |> Repo.insert()
    else
      {:invalid_changeset, :unprocessable_entity}
    end
  end

  @doc """
  Updates a account.

  ## Examples

      iex> update_account(account, %{field: new_value})
      {:ok, %Account{}}

      iex> update_account(account, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """

  # def update_account(%Account{} = account, attrs) do
  #   account
  #   |> Account.changeset(attrs)
  #   |> Repo.update()
  # end

  @doc """
  Deletes a account.

  ## Examples

      iex> delete_account(account)
      {:ok, %Account{}}

      iex> delete_account(account)
      {:error, %Ecto.Changeset{}}

  """

  # def delete_account(%Account{} = account) do
  #   Repo.delete(account)
  # end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking account changes.

  ## Examples

      iex> change_account(account)
      %Ecto.Changeset{source: %Account{}}

  """

  # def change_account(%Account{} = account) do
  #   Account.changeset(account, %{})
  # end

  alias EWallet.Accounts.Transaction

  @doc """
  Returns the list of transactions.

  ## Examples

      iex> list_transactions()
      [%Transaction{}, ...]

  """
  def list_transactions do
    Repo.all(Transaction)
  end

  @doc """
  Gets a single transaction.

  Raises `Ecto.NoResultsError` if the Transaction does not exist.

  ## Examples

      iex> get_transaction!(123)
      %Transaction{}

      iex> get_transaction!(456)
      ** (Ecto.NoResultsError)

  """
  def get_transaction!(id), do: Repo.get!(Transaction, id)

  @doc """
  Creates a transaction.

  ## Examples

      iex> create_transaction(%{field: value})
      {:ok, %Transaction{}}

      iex> create_transaction(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_transaction(%Account{} = account, attrs \\ %{}) do
    changeset = Transaction.changeset(%Transaction{}, attrs)
    attrs = process_attrs(attrs)

    balance = get_new_balance(account, attrs)

    with {:ok, true} <- sufficiant_balance?(balance),
         {:ok, true} <- valid_reference_if_any?(attrs),
         true <- changeset.valid? do
      Repo.transaction(fn ->
        change(account, balance)
        |> Repo.update!()

        Ecto.build_assoc(account, :transactions, attrs)
        |> Repo.insert!()
      end)
    else
      {:error, reason} ->
        {:error, reason}
      false ->
        IO.inspect(changeset.errors)
        {:error, "Bad Request"}
    end
  end

  defp get_new_balance(account, %{type: type} = attrs) when type in ["load"],
    do: %{
      available_balance: account.available_balance + attrs.amount,
      current_balance: account.current_balance + attrs.amount
    }

  defp get_new_balance(account, %{type: type} = attrs) when type in ["refund"],
    do: %{
      available_balance: account.available_balance + attrs.amount,
      current_balance: account.current_balance + attrs.amount
    }

  defp get_new_balance(account, %{type: type} = attrs) when type in ["void"],
    do: %{available_balance: account.available_balance + attrs.amount}

  defp get_new_balance(account, %{type: type} = attrs) when type in ["pre-auth"],
    do: %{available_balance: account.available_balance - attrs.amount}

  defp get_new_balance(account, %{type: type} = attrs) when type in ["sale"] do
    if Map.has_key?(attrs, "reference_id") do
      %{available_balance: account.available_balance - attrs.amount}
    else
      %{
        available_balance: account.available_balance - attrs.amount,
        current_balance: account.current_balance - attrs.amount
      }
    end
  end

  defp sufficiant_balance?(balance) do
    if (balance.available_balance >= 0) do
      {:ok, true}
    else
      {:error, "Insufficiant Balance"}
    end
  end

  defp valid_reference_if_any?(attrs) do
    if (Map.has_key?(attrs, :reference_id)) do
        t = Repo.get(Transaction, attrs.reference_id)
        cond do
          t == nil -> {:error, "Invalid reference_id"}
          t.type == "sales" && attrs.type == "refund" -> {:ok, true}
          t.type == "pre-auth" && attrs.type == "void" -> {:ok, true}
          true -> {:error, "Invalid reference_id"}
        end
    else
      if(attrs.type == "refund" || attrs.type == "void") do
        {:error, "reference_id is required"}
      else
        {:ok, true}
      end
    end
  end

  @doc """
  Updates a transaction.

  ## Examples

      iex> update_transaction(transaction, %{field: new_value})
      {:ok, %Transaction{}}

      iex> update_transaction(transaction, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """

  # def update_transaction(%Transaction{} = transaction, attrs) do
  #   transaction
  #   |> Transaction.changeset(attrs)
  #   |> Repo.update()
  # end

  @doc """
  Deletes a transaction.

  ## Examples

      iex> delete_transaction(transaction)
      {:ok, %Transaction{}}

      iex> delete_transaction(transaction)
      {:error, %Ecto.Changeset{}}

  """

  # def delete_transaction(%Transaction{} = transaction) do
  #   Repo.delete(transaction)
  # end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking transaction changes.

  ## Examples

      iex> change_transaction(transaction)
      %Ecto.Changeset{source: %Transaction{}}

  """

  # def change_transaction(%Transaction{} = transaction) do
  #   Transaction.changeset(transaction, %{})
  # end

  defp process_attrs(attrs),
    do: for({key, val} <- attrs, into: %{}, do: {String.to_atom(key), val})
end
