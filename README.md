# EWallet

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Seed DB with  `mix run priv/repo/seeds.exs`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

## Assumptions
  * There are No partial transactions
  * Referenced transactions will only be recieved once
  * Account is Wallet
  * Transaction routes are not authorized keeping in mind that the 3rd party vendors shall be using these
  * The commented code can be reused in future
  * Amount 2000 means $20.00 (Integers are being used to avoid precision issues faced with decimals and floats)

## End Points
  * Login 
    ```
    POST
    Request: /api/sign_in
    Header: [authorization, Bearer [token from login]]
    ```
  * Logout
    ```
    POST
    Request: /api/sign_out
    Request body:
    {
      "email": "aqdasmalikcs@gmail.com",
      "password": "12345678"
    }
    ```
  * Create account
    ```
      POST
      Request: /api/accounts
      Header: [authorization, Bearer [token from login]]
    ```
  * Create Transactions
    ```
    POST
    Request: /api/transactions
    Request body:
    {
      "type": "refund", // ["refund", "sale", "void", "pre-auth", "load"]
      "amount": 2000,
      "description": "Adding balance",
      "account_id": 1,
      "reference_id": 6 // only required when type in ["void", "refund"] 
    }
    ```
